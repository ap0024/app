import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'search',
    pure: false
})
export class SearchPipe implements PipeTransform {

    transform(value: any, query: Array<string>, field: any): any {
        let result = query.length > 0 ? value.reduce((prev, next) => {
            let push = false;
            query.forEach(item => {
                if (field in next && 
                    next[field].length > 0 && 
                    next[field].indexOf(item) != -1) {
                    push = true;
                }
            });
            if (push) {
                prev.push(next);
            }
            return prev;
        }, []) : value;

        return result;
    }

}
