import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'date'
})
export class DateFormatPipe implements PipeTransform {
    // Autocapitalizes the first letter of each word in a phrase.
    // Input: {{'2019-01-01' | date = ‘dd/MM/yyyy’}}
    // Output: John Doe
  transform(value: string, format: string) {
    var datePipe = new DatePipe("en-US");
    value = datePipe.transform(value, format || 'dd/MM/yyyy');
    return value;
  }
}
