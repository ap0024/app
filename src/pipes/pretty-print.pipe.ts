import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'prettyPrint'
})
export class PrettyPrint implements PipeTransform {

    transform(str: string) {
        return str && str.length > 0 ? str.replace(/(\r\n|\n\r|\r|\n)/gm, "<br />") : '';
    }

}
