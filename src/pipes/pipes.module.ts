import { NgModule } from '@angular/core';

import { TermSearchPipe } from './term-search'
import { CapitalizePipe } from './capitalize.pipe';
import { OrderByPipe } from './orderby.pipe';
import { ShortenStringPipe } from './shorten.pipe';
import { TemperaturePipe } from './temperature.pipe';
import { SearchPipe } from './search.pipe';
import { PrettyPrint } from './pretty-print.pipe';
import { DateFormatPipe } from './date.pipe';

export const pipes = [
    TermSearchPipe,
    CapitalizePipe,
    OrderByPipe,
    ShortenStringPipe,
    TemperaturePipe,
    SearchPipe,
    PrettyPrint,
    DateFormatPipe
];

@NgModule({
    declarations: [pipes],
    exports: [pipes]
})

export class PipesModule { }
