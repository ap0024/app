import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProfileService } from './profile-service';
import { api } from './config';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class BannersService {
    banner: any = {};

    constructor(
        public http: HttpClient,
        public profileService: ProfileService, 
        public translate: TranslateService
    ) {

    }

    getItem() {

        return new Promise<any>(resolve => {
            this.http.get<any>(api.SERVER_URL + api.ENDPOINTS.BANNER, {
                headers: {'Accept-language': this.translate.currentLang}
             }).subscribe(data => {
                    if (data != undefined && !data["ERROR"]) {
                        this.banner = data;
                        resolve(this.banner);
                    }
                }, err => {
                    console.log(err);
                    resolve([])
                }
            );
        })
    }


}
