import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { api } from './config';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class RestaurantService {

    favoriteCounter: number;
    favorites: Array<any>;
    restaurants: any[];
    loading: boolean = false;
    timeout: number = 10;
    distance: number = 30; //30 km

    constructor(public http: HttpClient, public translate: TranslateService) { }

    findAll(lng = undefined, lat = undefined) {
        console.log(api.SERVER_URL + ((lng != undefined && lat != undefined) ?
            [api.ENDPOINTS.RESTAURANTS, lat, lng, this.distance].join("/")
            : api.ENDPOINTS.RESTAURANTS));
        return new Promise<any[]>(resolve => {
            if (!this.loading) {
                this.loading = true;
                this.http.get<[any]>(api.SERVER_URL + ((lng != undefined && lat != undefined) ?
                    [api.ENDPOINTS.RESTAURANTS, lat, lng, this.distance].join("/")
                    : api.ENDPOINTS.RESTAURANTS), {
                        headers: { 'Accept-language': this.translate.currentLang }
                    }).subscribe(data => {
                        this.loading = false;
                        if (data != undefined) {
                            this.restaurants = data;
                            console.log('restaurants success')
                        }
                        resolve(this.restaurants);
                    }, err => {
                        resolve(this.restaurants);
                    }
                    );
            } else {
                console.log('timeout', this.timeout)
                if (this.timeout > 0) setTimeout(() => { this.timeout--; this.findAll() }, 1000);
                // resolve(this.restaurants);
            }
        });
    }

    getItem(id) {
        return new Promise<any>(resolve => {
            this.http.get<any>(api.SERVER_URL +
                [api.ENDPOINTS.RESTAURANTS, id].join('/'), {
                    headers: { 'Accept-language': this.translate.currentLang }
                }).subscribe(data => {
                    this.loading = false;
                    if (data != undefined) {
                        this.restaurants = data;
                        resolve(data);
                    }
                }, err => {
                    console.log(err);
                }
                );
        })
    }


    getOffer(restaurant, offerId) {
        console.log('getOffer', { restaurant, offerId })
        return new Promise(resolve => {
            restaurant && restaurant.offers.filter(o => {
                if (o.id === parseInt(offerId)) {
                    resolve(o);
                }
            })
        })
    }

    findByName(searchKey: string) {
        console.log('findByName')
        let key: string = searchKey.toLowerCase();
        return new Promise<any[]>(resolve => this.findAll().then(restaurants => resolve(restaurants.filter((restaurant: any) =>
            (restaurant.title + ' ' + restaurant.address + ' ' + restaurant.city + ' ' + restaurant.description).toLowerCase().indexOf(key) > -1))));
    }

    getFavorites() {
        return Promise.resolve(this.favorites);
    }

    favorite(restaurant) {
        this.favoriteCounter = this.favoriteCounter + 1;
        this.favorites.push({ id: this.favoriteCounter, restaurant: restaurant });
        return Promise.resolve();
    }

    unfavorite(favorite) {
        let index = this.favorites.indexOf(favorite);
        if (index > -1) {
            this.favorites.splice(index, 1);
        }
        return Promise.resolve();
    }

}
