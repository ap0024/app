let dishes: Array<any> = [
    {
        id: 1,
        name: "Crema de calabaza",
        ingredients: "Calabaza, papas, sal, agua",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus at consequuntur dolores, ea reprehenderit ipsam voluptas nulla recusandae.",
        picture: "assets/img/dishes/dish01.jpg",
        images: [
            "assets/img/dishes/dish01.jpg",
            "assets/img/dishes/dish03.jpg",
            "assets/img/dishes/dish04.jpg",
            "assets/img/dishes/dish05.jpg",
        ],
        price: 6.5,
        qtd: 0
    },
    {
        id: 2,
        name: "Sopa de pescado",
        ingredients: "Pescado, papas, sal, agua",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus at consequuntur dolores, ea reprehenderit ipsam voluptas nulla recusandae.",
        picture: "assets/img/dishes/dish02.jpg",
        images: [
            "assets/img/dishes/dish02.jpg",
            "assets/img/dishes/dish04.jpg",
            "assets/img/dishes/dish05.jpg",
            "assets/img/dishes/dish06.jpg",
        ],
        price: 7.50,
        qtd: 0
    },
    {
        id: 3,
        name: "Fabada",
        ingredients: "Judías, papas, sal, agua",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus at consequuntur dolores, ea reprehenderit ipsam voluptas nulla recusandae.",
        picture: "assets/img/dishes/dish03.jpg",
        images: [
            "assets/img/dishes/dish03.jpg",
            "assets/img/dishes/dish02.jpg",
            "assets/img/dishes/dish04.jpg",
            "assets/img/dishes/dish05.jpg",
        ],
        price: 6.00,
        qtd: 0
    },
    {
        id: 4,
        name: "Cocido a la madrileña",
        ingredients: "Verdudas, papas, sal, agua",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus at consequuntur dolores, ea reprehenderit ipsam voluptas nulla recusandae.",
        picture: "assets/img/dishes/dish04.jpg",
        images: [
            "assets/img/dishes/dish04.jpg",
            "assets/img/dishes/dish06.jpg",
            "assets/img/dishes/dish07.jpg",
            "assets/img/dishes/dish08.jpg",
        ],
        price: 8.00,
        qtd: 0
    }
];

export default dishes;
