import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { api, sysOptions } from './config';
import { OneSignal } from '@ionic-native/onesignal';
import {TranslateService} from '@ngx-translate/core';


@Injectable()
export class ProfileService {

    expiryKey: string = 'EXPIRY_TOKEN';
    tokenKey: string = 'API_TOKEN';
    userKey: string = 'USER_DATA';
    expiryDays: number = 365;

    currentUser: any = {};

    constructor(
        public http: HttpClient,
        public oneSignal: OneSignal, 
        public translate: TranslateService
    ) {
        this.expiryToken();
    }

    /**
     * Logout if expiry time exceeded
     */
    expiryToken() {
        if (localStorage.getItem(this.expiryKey) && 
            new Date().getTime() <= parseInt(localStorage.getItem(this.expiryKey))) {
            return;
        }
        this.logout();
    }

    getCurrentUserToken() {
        return localStorage.getItem(this.tokenKey)
    }

    /**
     * Trying to get user from LocalStorage without expiration time
     * If Token exists without user, trying to retreive data from Api
     * If Token expired or user don't exists make LOGOUT
     */
    getCurrentUser() {
        if (this.currentUser && this.currentUser.length > 0) {
            return Promise.resolve(this.currentUser);

        } else {
            let userJSON = localStorage.getItem(this.userKey);
            if (userJSON) {
                this.assignDto(JSON.parse(userJSON));
                return Promise.resolve(this.currentUser);
            } else if (this.getCurrentUserToken()) {
                return new Promise(resolve => {
                    this.http.get<[any]>(api.URL + api.ENDPOINTS.USERTOKEN + 
                        this.getCurrentUserToken(), {
                            headers: {'Accept-language': this.translate.currentLang}
                    }).subscribe(data => {
                        if ("ERROR" in data) {
                            this.logout();

                        } else {
                            localStorage.setItem(this.userKey, JSON.stringify(data));
                            this.assignDto(data);
                            
                        }
                        resolve(this.currentUser);
                    });
                })
            } else {
                return Promise.resolve(null);
            }
        }
    }


    /**
     * Update user function
     * 
     * Sending data to Api
     * and updating currentUser in localStorage
     * 
     * @param data 
     */
    update(currentUser) {
        return new Promise(resolve => {
            this.http.put<[any]>(api.URL + 
                api.ENDPOINTS.UPDATE_PROFILE + 
                this.currentUser.id, this.assignApiData(currentUser))
                .subscribe(data => {
                        if (!data['ERROR']) {
                            this.refreshCurrentUser();
                        }
                        resolve(data);
                },
                error => {
                    resolve(error.error);
                });
        });

    }

    /**
     * Send email with verivication token
     * 
     * @param email
     */
    forgotPass(email) {
        return new Promise(resolve => {
            this.http.post<[any]>(api.SERVER_URL + api.ENDPOINTS.FORGOT_PASSWORD, {email})
                .subscribe(data => {
                    resolve(data);
                },
                error => {
                    resolve(error.error);
                });
        });
    }

    /**
     * Verify token
     * @param data: {token, email} 
     */
    verifyToken(data) {
        return new Promise(resolve => {
            this.http.post<[any]>(api.SERVER_URL + 
                api.ENDPOINTS.RESET_PASSWORD + '/' + data.token, {email:data.email} )
                .subscribe(data => {
                    resolve(data);
                },
                error => {
                    resolve(error.error);
                });
        });
    }

    /**
     * Verify token
     * @param data: {token, email, password, password_confirmation} 
     */
    resetPassword(data) {
        return new Promise(resolve => {
            this.http.post<[any]>(api.SERVER_URL + api.ENDPOINTS.RESET_PASSWORD, data)
                .subscribe(data => {
                    if ("data" in data) {
                        this.loginData(data['data']);
                        resolve(this.currentUser);
                    } else {
                        resolve(data);
                    }
                },
                error => {
                    resolve(error.error);
                });
        });
    }

    /**
     * Register user function
     * 
     * Sending data to Api and instant login with token
     * 
     * @param data 
     */
    register(data) {
        return new Promise(resolve => {
            this.http.post<[any]>(api.URL + 
                api.ENDPOINTS.REGISTER, this.assignRegistrationData(data))
                .subscribe(data => {
                    if ("data" in data) {
                        this.loginData(data['data']);
                        resolve(this.currentUser);
                    } else {
                        resolve(data);
                    }
                },
                error => {
                    resolve(error.error);
                });
        });

    }

    /**
     * Register with facebook details info
     * 
     * @param userId 
     * @param data: {id, email, name}
     */
    registerFaceBook(userId, data) {
        return new Promise(resolve => {
            this.http.post<[any]>(api.URL + 
                api.ENDPOINTS.REGISTER, this.assignFacebookData(userId, data))
                .subscribe(data => {
                    if ("data" in data) {
                        this.loginData(data['data']);
                        resolve(this.currentUser);
                    } else {
                        resolve(data);
                    }
                },
                error => {
                    resolve(error.error);
                });
        });
    }

    /**
     * Login with Fcebook Id
     * @param userId
     * @param data
     */
    loginFaceBook(userId, data) {
        return new Promise(resolve => {
            this.http.post<[any]>(api.SERVER_URL + api.ENDPOINTS.LOGIN_FB + '/' + userId, data)
                .subscribe(data => {
                    this.loginData(data);
                    resolve(this.currentUser)
                },
                error => {
                    resolve(error);
                });
        });
    }

    /**
     * Login function
     * 
     * Saving in local storage
     *  - Token
     *  - Current User Data
     *  - Expiry Time
     * 
     * @param data 
     */
    login(data) {
        return new Promise(resolve => {
            this.http.post<[any]>(api.URL + api.ENDPOINTS.LOGIN, data)
                .subscribe(data => {
                    this.loginData(data);
                    resolve(this.currentUser)
                },
                error => {
                    resolve(error);
                });
        });

    }

    logout() {
        this.assignDto(null);
        localStorage.removeItem(this.tokenKey);
        localStorage.removeItem(this.userKey);
        localStorage.removeItem(this.expiryKey);
    }

    refreshCurrentUser() {
        this.currentUser = null;
        localStorage.removeItem(this.userKey);
        this.getCurrentUser();
    }

    assignApiData(currentUser) {
        let data = {
            executorToken: this.getCurrentUserToken(),
            email: currentUser.email,
            USER_STATUS_ID: 1,
            password: currentUser.password,
            newPassword: currentUser.newPassword,
            USER_DATA: {
                DATA_NAME: currentUser.firstName,
                DATA_SURNAME_01: currentUser.lastName,
                DATA_BIRTHDATE: this.parseBirthDay(currentUser.birthDay),
                DATA_AVATAR: currentUser.picture,
                DATA_ADDRESS: currentUser.address,
                DATA_LANGUAGE: currentUser.language

            }
        }
        return data;
    }

    assignDto(rawUser) {
        this.currentUser = {};
        if (rawUser && rawUser[this.userKey]) {
            this.currentUser.id = rawUser['USER_ID'];
            this.currentUser.picture = rawUser[this.userKey]['DATA_AVATAR'];
            this.currentUser.address = rawUser[this.userKey]['DATA_ADDRESS'];
            this.currentUser.firstName =  rawUser[this.userKey]['DATA_NAME'];
            this.currentUser.lastName = rawUser[this.userKey]['DATA_SURNAME_01'];
            this.currentUser.username = this.currentUser.firstName + (this.currentUser.lastName && this.currentUser.lastName.length > 0 ? ' ' + this.currentUser.lastName : '');
            this.currentUser.email = rawUser['email'];
            this.currentUser.birthDay = this.parseRawBirthDateIso(rawUser[this.userKey]['DATA_BIRTHDATE']);
            this.currentUser.language = rawUser[this.userKey]['DATA_LANGUAGE'] || sysOptions.systemLanguage;
        } 
    }

    parseBirthDay(birthDay) {
        if (birthDay && birthDay.length > 0) {
            var dt = new Date(birthDay);
            return dt.getFullYear() + '-' + (dt.getMonth()+ 1) +'-' + ("0" + dt.getDate()).slice (-2) ;
        }
    }

    parseRawBirthDateIso(rawDate) {
        if (rawDate && rawDate.length > 0) {
            var dt = new Date(rawDate);
            return dt.toISOString();
        }
    }

    assignRegistrationData(data) {
        let names = data.fullName.split(' ');
        return {
            'USER_ROLE_ID': 1,
            'email': data.email,
            'password': data.password,
            'password_confirmation': data.password,
            'USER_DATA': {
                'DATA_NAME': names[0],
                'DATA_SURNAME_01': names.length > 1 ? names.slice(1).join(" ") : ''
            }
        }
    }

    loginData(data) {
        if (this.tokenKey in data && data[this.tokenKey].length > 0) {
            localStorage.setItem(this.tokenKey, data[this.tokenKey]);

            var expiryDate = new Date();
            expiryDate.setDate(expiryDate.getDate()+this.expiryDays);
            localStorage.setItem(this.expiryKey, expiryDate.getTime().toString());

            if (this.userKey in data) {
                localStorage.setItem(this.userKey, JSON.stringify(data[this.userKey]));
                this.assignDto(data[this.userKey]);
            }

            this.oneSignal.getIds().then((data) => {
                console.log("OneSignal Ids", data);
                this.updateOneSignal(data.userId);
            });
        }
    }

    assignFacebookData(userId, user) {
        let password = this.generateRandomPassword(6);
        let names = user.name.split(' ');
        return {
            'USER_ROLE_ID': 1,
            'email': user.email,
            'password': password,
            'password_confirmation': password,
            'USER_DATA': {
                'DATA_FACEBOOK_ID': userId,
                'DATA_NAME': names[0],
                'DATA_SURNAME_01': names.length > 1 ? names.slice(1).join(" ") : '',
                'DATA_AVATAR': "https://graph.facebook.com/" + userId + "/picture?type=large"
            }
        }
    }

    generateRandomPassword(length) {
        return Math.random().toString(36).substring(2, length-1) + Math.random().toString(36).substring(2, length-1);
    }

    updateOneSignal(userId) {
        let token = this.getCurrentUserToken();
        return new Promise(resolve => {
            this.http.post<[any]>(api.SERVER_URL + api.ENDPOINTS.ONESIGNAL, {userId, token})
                .subscribe(data => {
                    resolve(data);
                },
                error => {
                    resolve(error.error);
                });
        });
    }

}
