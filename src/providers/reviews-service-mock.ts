import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { ProfileService } from './profile-service';
import { api } from './config';

@Injectable()
export class ReviewsService {

    reviewCounter: number = 0;
    reviews: Array<any> = [];

    constructor(
        public http: HttpClient,
        public profileService: ProfileService
    ) {

    }

    saveReview(offerId, data) {
        return new Promise(resolve => {
            this.http.post(api.SERVER_URL + api.ENDPOINTS.REVIEWS, this.assignApiData(offerId, data))
                .subscribe(data => {
                    resolve(data);
                },
                error => {
                    resolve(error.error);
                });
        });
    }

    assignApiData(offerId, data) {
        return {
            "orderId": offerId, 
            "rating": data.rate, 
            "review": data.message, 
            "token": this.profileService.getCurrentUserToken()
        }
    }

}
