import { Injectable } from '@angular/core';
// import restaurants from './mock-restaurants';

@Injectable()
export class CartService {

    orderCounter: number = 0;
    orders: Array<any> = [];

    addtoCart(order, qtd) {
        this.orderCounter = this.orderCounter + 1;
        this.orders.push({ id: this.orderCounter, order: order, qtd: qtd });
        return Promise.resolve();
    }

    getOrders() {
        this.orders = [
            {
                id: 1,
                onumber: 2254,
                order: [
                    {
                        id: 1,
                        order: [],
                        qtd: 2
                    }
                ],
                total: 13
            }
        ]
        return Promise.resolve(this.orders);
    }

    removefromCart(order) {
        let index = this.orders.indexOf(order);
        if (index > -1) {
            this.orders.splice(index, 1);
        }
        return Promise.resolve();
    }

    editQtdOrder(order, op) {
        for (let i in this.orders) {
            if (this.orders[i].id === order.id) {
                if (op === 'minus') {
                    this.orders[i].qtd--;
                    break;
                }
                if (op === 'plus') {
                    this.orders[i].qtd++;
                    break;
                }
            }
        }
        return Promise.resolve();
    }

    cleanCart() {
        this.orders = [];
    }

}
