export let api: any = {
	URL: 'http://api.terracy.com/api/',
	SERVER_URL: 'http://api.terracy.com/api/v2/',
	countriesApi: 'https://restcountries.eu/rest/v2/all',
	ENDPOINTS: {
		RESTAURANTS: 'restaurants',
		TYPES: 'types',
		CHARACTERISTICS: 'characteristics',
		LOGIN: 'login',
		LOGIN_FB: 'login/fb',
		REGISTER: 'register',
		USERTOKEN: 'users/bytoken/',
		UPDATE_PROFILE: 'users/',
		RESET_PASSWORD: 'password/reset',
		FORGOT_PASSWORD: 'password/email',
		ORDERS: 'orders',
		ORDERS_STOCKS: 'orders/stock',
		ORDER_DELETE: 'orders/delete',
		ORDER_CONFIRM: 'orders/confirm',
		BOOK: 'orders/book',
		ORDER_CANCEL: 'orders/cancel',
		REVIEWS: 'reviews',
		PROMOTIONS: 'promotions',
		PROMOTIONS_STOCKS: 'promotions/stock',
		PROMOTIONS_CANCEL: 'promotions/cancel',
		PROMOTIONS_DELETE: 'promotions/delete',
		PROMOTIONS_CONFIRM: 'promotions/confirm',
		ONESIGNAL: 'onesignal',
		BANNER: 'banners/primero'
	}
}

export const availableLanguages = [{
	code: 'en',
	name: 'English'
},{
	code: 'es',
	name: 'Español'
}, {
	code: 'de',
	name: 'Deutsch'
}, {
	code: 'it',
	name: 'Italiano'
}, {
	code: 'fr',
	name: 'Francais'
}];


export const defaultLanguage = 'en';

export const sysOptions = {
	systemLanguage: defaultLanguage
};
