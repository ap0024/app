import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { api } from './config';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class FilterService {
  
  prices: any = [];
  types: any = [];
  characteristics: any = [];
 
  filters: any = {'price': [], 'types': [], 'characteristics': []};

  constructor(
    public http: HttpClient, 
    public translate: TranslateService
    ) {
    this.prices = ['€','€€','€€€'];
    this.types = [];
    this.characteristics = [];
  }
 
  loadTypes() {
    if (this.types && this.types.length > 0) {
      return Promise.resolve(this.types);
    }
 
    return new Promise(resolve => {
      this.http.get<[any]>(api.SERVER_URL + api.ENDPOINTS.TYPES, {
        headers: {'Accept-language': this.translate.currentLang}
      }).subscribe(data => {
          this.types = data && data.length > 0 && data.map(d => d.name);
          resolve(this.types);
        });
    });
  }

  loadCharacteristics() {
    if (this.characteristics && this.characteristics.length > 0) {
      return Promise.resolve(this.characteristics);
    }
 
    return new Promise(resolve => {
      this.http.get<[any]>(api.SERVER_URL + api.ENDPOINTS.CHARACTERISTICS, {
        headers: {'Accept-language': this.translate.currentLang}
      }).subscribe(data => {
          this.characteristics = data && data.length > 0 && data.map(d => d.name);
          resolve(this.characteristics);
        });
    });
  }
}