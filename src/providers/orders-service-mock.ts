import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProfileService } from './profile-service';
import { api } from './config';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class OrdersService {

    orderCounter: number = 0;
    orders: any[] = [];
    stocks = [];

    constructor(
        public http: HttpClient,
        public profileService: ProfileService,
        public translate: TranslateService
    ) { }

    saveOrder_(order, total, orderNumber) {
        return Promise.resolve();
    }

    orderOffer(restaurantId, offerId, date, time, qty) {
        return new Promise(resolve => {
            this.http.post<[any]>(api.SERVER_URL +
                [api.ENDPOINTS.PROMOTIONS, restaurantId, offerId].join("/"),
                this.assignApiOffer(qty, date, time))
                .subscribe(data => {
                    resolve(data);
                },
                    error => {
                        resolve(error.error);
                    });
        });
    }

    orderTables(restaurantId, date, time, pax) {
        return new Promise(resolve => {
            this.http.post<[any]>(api.SERVER_URL + api.ENDPOINTS.BOOK + "/"
                + restaurantId, this.assignApiTables(pax, date, time))
                .subscribe(data => {
                    resolve(data);
                },
                    error => {
                        resolve(error.error);
                    });
        });
    }

    getAllOrders() {
        return new Promise<any[]>(resolve => {
            this.http.get<[any]>(api.SERVER_URL +
                [api.ENDPOINTS.ORDERS, this.profileService.getCurrentUserToken()].join("/"), {
                    headers: { 'Accept-language': this.translate.currentLang }
                }).subscribe(data => {
                    if (data != undefined && !data["ERROR"]) {
                        this.orders = data;
                        resolve(this.orders);
                    }
                }, err => {
                    console.log(err);
                    resolve([])
                }
                );
        })
    }

    cancelOrder(id) {
        return new Promise(resolve => {
            this.http.get<[any]>(api.SERVER_URL +
                [api.ENDPOINTS.ORDER_CANCEL, id, this.profileService.getCurrentUserToken()].join("/"), {
                    headers: { 'Accept-language': this.translate.currentLang }
                }).subscribe(data => {
                    resolve(data);
                }, err => {
                    resolve(err.error);
                })
        })
    }
    deleteOrder(id) {
        return new Promise(resolve => {
            this.http.get<[any]>(api.SERVER_URL +
                [api.ENDPOINTS.ORDER_DELETE, id, this.profileService.getCurrentUserToken()].join("/"), {
                    headers: { 'Accept-language': this.translate.currentLang }
                }).subscribe(data => {
                    resolve(data);
                }, err => {
                    resolve(err.error);
                })
        })
    }
    cancelOffer(id) {
        return new Promise(resolve => {
            this.http.get<[any]>(api.SERVER_URL +
                [api.ENDPOINTS.PROMOTIONS_CANCEL, id, this.profileService.getCurrentUserToken()].join("/"), {
                    headers: { 'Accept-language': this.translate.currentLang }
                }).subscribe(data => {
                    resolve(data);
                }, err => {
                    resolve(err.error);
                })
        })
    }
    confirmOffer(id) {
        return new Promise(resolve => {
            this.http.get<[any]>(api.SERVER_URL +
                [api.ENDPOINTS.PROMOTIONS_CONFIRM, id, this.profileService.getCurrentUserToken()].join("/"), {
                    headers: { 'Accept-language': this.translate.currentLang }
                }).subscribe(data => {
                    resolve(data);
                }, err => {
                    resolve(err.error);
                })
        })
    }
    confirmOrder(id) {
        return new Promise(resolve => {
            this.http.get<[any]>(api.SERVER_URL +
                [api.ENDPOINTS.ORDER_CONFIRM, id, this.profileService.getCurrentUserToken()].join("/"), {
                    headers: { 'Accept-language': this.translate.currentLang }
                }).subscribe(data => {
                    resolve(data);
                }, err => {
                    resolve(err.error);
                })
        })
    }
    deleteOffer(id) {
        return new Promise(resolve => {
            this.http.get<[any]>(api.SERVER_URL +
                [api.ENDPOINTS.PROMOTIONS_DELETE, id, this.profileService.getCurrentUserToken()].join("/"), {
                    headers: { 'Accept-language': this.translate.currentLang }
                }).subscribe(data => {
                    resolve(data);
                }, err => {
                    resolve(err.error);
                })
        })
    }

    getItem() {

    }

    getTables(restaurantId, showDays) {
        let now = new Date();
        let startDate = now.toISOString().substr(0, 10);
        let a = now.setDate(now.getDate() + showDays);
        let b = new Date(a);
        let endDate = b.toISOString().substr(0, 10);

        return new Promise<any[]>(resolve => {
            this.http.get<any[]>(api.SERVER_URL +
                [api.ENDPOINTS.ORDERS_STOCKS, restaurantId, startDate, endDate].join("/"), {
                    headers: { 'Accept-language': this.translate.currentLang }
                }).subscribe((data) => {
                    this.stocks = data;
                    resolve(this.stocks);
                }, err => {
                    console.log(err);
                    resolve(err.error);
                }
                );
        })
    }

    getPromotionStock(restaurantId, offerId, showDays) {
        let now = new Date();
        let startDate = now.toISOString().substr(0, 10);
        let a = now.setDate(now.getDate() + showDays);
        let b = new Date(a);
        let endDate = b.toISOString().substr(0, 10);

        return new Promise<any[]>(resolve => {
            this.http.get<any[]>(api.SERVER_URL +
                [api.ENDPOINTS.PROMOTIONS_STOCKS, restaurantId, offerId, startDate, endDate].join("/"), {
                    headers: { 'Accept-language': this.translate.currentLang }
                }).subscribe((data) => {
                    this.stocks = data;
                    resolve(this.stocks);
                }, err => {
                    console.log(err);
                    resolve(err.error);
                }
                );
        })
    }


    assignApiTables(pax, date, time) {
        return {
            token: this.profileService.getCurrentUserToken(),
            pax,
            date: date + " " + time + ":00"
        }
    }

    assignApiOffer(qty, date, time) {
        let token = this.profileService.getCurrentUserToken();
        return { token, qty, date: date + " " + time + ":00" }
    }

}
