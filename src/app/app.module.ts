import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { AgmOverlays } from "agm-overlays";
import { AgmCoreModule } from '@agm/core';

import { TerracyApp } from './app.component';

import { PipesModule } from '../pipes/pipes.module';

import { MessageService } from "../providers/message-service-mock";
import { RestaurantService } from "../providers/restaurant-service-mock";
import { DishService } from "../providers/dish-service-mock";
import { CategoryService } from "../providers/category-service-mock";
import { CartService } from "../providers/cart-service-mock";
import { OrdersService } from "../providers/orders-service-mock";
import { BannersService } from "../providers/banners-service";
import { ProfileService } from "../providers/profile-service";
import { FilterService } from "../providers/filter-service";
import { ReviewsService } from "../providers/reviews-service-mock"

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';
import { Facebook } from '@ionic-native/facebook';

import { OneSignal } from '@ionic-native/onesignal';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
    declarations: [
        TerracyApp
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        IonicModule.forRoot(TerracyApp, {
            preloadModules: true,
            scrollPadding: false,
            scrollAssist: true,
            autoFocusAssist: false,
            mode: 'md'
        }),
        IonicStorageModule.forRoot({
            name: '__foodIonicDB',
            driverOrder: ['indexeddb', 'sqlite', 'websql']
        }),
        AgmOverlays,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyA9KsxPhnyl66IfUnGE_uYzw5fuSW0E5M4'
        }),
        PipesModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        TerracyApp
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Keyboard,
        RestaurantService,
        DishService,
        CategoryService,
        MessageService,
        CartService,
        OrdersService,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        ProfileService,
        FilterService,
        Facebook,
        ReviewsService,
        OneSignal,
        BannersService
    ]
})
export class AppModule { }
