import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProfileService } from '../providers/profile-service';
import { Events } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';
import { TranslateService } from '@ngx-translate/core';
import { defaultLanguage, availableLanguages, sysOptions } from '../providers/config';

export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
    templateUrl: 'app.html'
})
export class TerracyApp {
    @ViewChild(Nav) nav: Nav;

    tabsPlacement: string = 'bottom';
    tabsLayout: string = 'icon-top';

    // rootPage: any = 'page-restaurant-list';
    rootPage: any = localStorage.getItem('tutorial') != '1' ? 'page-walkthrough' : 'page-auth';
    showMenu: boolean = true;

    homeItem: any;

    initialItem: any;

    messagesItem: any;

    settingsItem: any;

    appMenuItems: Array<MenuItem>;

    yourRestaurantMenuItems: Array<MenuItem>;

    accountMenuItems: Array<MenuItem>;

    helpMenuItems: Array<MenuItem>;

    currentUser: any;

    definedLanguageCode = localStorage.getItem('subscribe.toggleLanguage.definedLanguageCode');
    
    translation: any;

    constructor(
        public platform: Platform,
        public statusBar: StatusBar,
        public splashScreen: SplashScreen,
        public menuCtrl: MenuController,
        public profileService: ProfileService,
        public events: Events,
        public oneSignal: OneSignal,
        public translateService: TranslateService
 
    ) {
        this.initializeApp();

        this.menuCtrl.enable(true);

        this.homeItem = { component: 'page-home' };
        this.messagesItem = { component: 'page-message-list' };
    }

    initializeApp() {
        this.events.subscribe('toggleProfile', () => {
            this.profileService.getCurrentUser().then((user) => {
                this.currentUser = user;
                if (this.currentUser != undefined && this.currentUser.language != null) {
                    var language = this.getSuitableLanguage(this.currentUser.language);
                    this.translateService.use(language);
                    console.log("toggleProfile:translateService.use", language);
                    sysOptions.systemLanguage = language;
                }
                console.log('toggleProfile', user);
            });
        });

        this.events.subscribe('toggleLanguage', (toggleLanguage) => {
            this.setLanguage(toggleLanguage)
            localStorage.setItem('subscribe.toggleLanguage.definedLanguageCode', toggleLanguage);
            this.definedLanguageCode = toggleLanguage;
        });

        this.platform.ready().then(() => {
            //Language
            this.translateService.setDefaultLang(defaultLanguage);
            var language = defaultLanguage;
            if (this.definedLanguageCode) {
                language = this.definedLanguageCode;
                console.log("definedLanguageCode exists", language);
            } else {
                if (this.translateService.getBrowserLang()) {
                    language = this.translateService.getBrowserLang();
                    console.log("browserLanguage", language);
                }
            }

            this.setLanguage(language);

            this.translateService.get([
                'Inicio',
                'Mis reservas',
                'Configurar perfil',
                'Bienvenido',
                'Entrar',
                'Mi cuenta',
                'Cerrar sesión'
                ]).subscribe((res: string) => {
                    console.log('translations', res);
                    this.translation = res;

                    this.appMenuItems = [
                        { title: this.translation['Inicio'], component: 'page-restaurant-list', icon: 'home' },
                        { title: this.translation['Mis reservas'], component: 'page-orders', icon: 'list-box' },
                        { title: this.translation['Configurar perfil'], component: 'page-my-account', icon: 'contact' },
                        { title: this.translation['Bienvenido'], component: 'page-walkthrough', icon: 'wine' }
                    ];

                    this.accountMenuItems = [
                        { title: this.translation['Entrar'], component: 'page-auth', icon: 'log-in' },
                        { title: this.translation['Mi cuenta'], component: 'page-my-account', icon: 'contact' },
                        { title: this.translation['Cerrar sesión'], component: 'page-auth', icon: 'log-out' },
                    ];
            });

            this.statusBar.overlaysWebView(false);
            this.splashScreen.hide();
            this.events.publish('toggleProfile');

            if (this.platform.is('cordova')) {
                console.log('cordova')
                this.initOneSignal('b633fdfd-c5d9-4fbd-9eea-1a467ec6ce0b', '100474996187');
            }

        });

        if (!this.platform.is('mobile')) {
            this.tabsPlacement = 'top';
            this.tabsLayout = 'icon-left';
        }
    }

    setLanguage(toggleLanguage) {
        console.log('setLanguage', toggleLanguage);
        var language = this.getSuitableLanguage(toggleLanguage);
        this.translateService.use(language);
        sysOptions.systemLanguage = language;
    }

	getSuitableLanguage(language) {
		language = language.substring(0, 2).toLowerCase();
		return availableLanguages.some(x => x.code == language) ? language : defaultLanguage;
    }
    
    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }

    logout(page) {
        console.log('Logout')
        this.profileService.logout();
        this.nav.setRoot('page-auth');
    }

    initOneSignal(key, id) {
        this.oneSignal.startInit(key, id);
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.handleNotificationOpened().subscribe((data) => {
            this.nav.setRoot('page-orders').catch(e => {console.log(e)});
        });
        this.oneSignal.endInit();
    }
}
