import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { IonicPage, NavController, LoadingController, ToastController } from 'ionic-angular';
import { ProfileService } from '../../providers/profile-service';
import { Events } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { availableLanguages } from '../../providers/config';

@IonicPage({
    name: 'page-my-account',
    segment: 'my-account'
})

@Component({
    selector: 'page-my-account',
    templateUrl: 'my-account.html'
})
export class MyAccountPage implements OnInit {

    profiledata: Boolean = true;
    public onProfileForm: FormGroup;
    picture: File;
    currentUser: any = null;
    loading: any = false;
    translation: any = {};
    languages = availableLanguages;

    constructor(
        private _fb: FormBuilder,
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public profileService: ProfileService,
        public events: Events,
        public translate: TranslateService
    ) { }

    ngOnInit() {
        this.translate.get([
            'Por favor, espere...',
            'Se ha actualizado la información.'
        ]).subscribe((res: string) => {
            this.translation = res;
        });
        this.onProfileForm = this._fb.group({
            firstName: [''],
            lastName: [''],
            address: [''],
            email: ['', Validators.compose([
                Validators.required
            ])],
            birthDay: [''],
            password: [''],
            newPassword: [''],
            language: ['']
        });
        this.profileService.getCurrentUser().then(
            (currentUser) => {
                this.currentUser = currentUser;
                console.log(this.currentUser);
            }
        );
    }

    // process send button
    sendData() {
        if (this.loading) {
            return;
        }
        this.loading = true;
        let loader = this.loadingCtrl.create({
            content: this.translation['Por favor, espere...']
        });
        // show message
        let toast = this.toastCtrl.create({
            showCloseButton: false,
            // cssClass: 'profiles-bg',
            cssClass: '',
            message: this.translation['Se ha actualizado la información.'],
            duration: 3000,
            position: 'top',
            closeButtonText: 'Cerrar'
        });

        loader.present();

        this.profileService.update(this.currentUser).then((result) => {
            this.loading = false;
            loader.dismiss();
            if (!result["ERROR"]) {
                toast.present();
                this.navCtrl.push('page-my-account');
                this.events.publish('toggleProfile');

            } else {
                // show error
                this.translate.get(result["ERROR"]).subscribe((error: string) => {
                    this.toastCtrl.create({
                        showCloseButton: false,
                        cssClass: '',
                        message: error,
                        duration: 3000,
                        position: 'top',
                    }).present();
                });
                toast.present();
            }
        });
    }

    imageUploadHandler() {
        console.log('imageUpload')
    }

    pictureChange($event): void {
        console.log($event, $event);
        var reader = new FileReader();
        reader.addEventListener("load", (event: any) => {
            this.currentUser.picture = event.target.result;
        }, false);

        if ($event.target.files && $event.target.files.length > 0) {
            let file = $event.target.files[0];
            reader.readAsDataURL(file);
        }
    }

    /**
     * Show datepicker for 10 years before
     */
    maxBirthDay() {
        let now = new Date();
        now.setDate(now.getDate() - 3650);
        return now.toISOString();
    }


}
