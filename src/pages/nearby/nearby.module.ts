import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgmCoreModule } from '@agm/core';
import { TranslateModule } from '@ngx-translate/core';

import { NearbyPage } from './nearby';

@NgModule({
    declarations: [
        NearbyPage
    ],
    imports: [
        IonicPageModule.forChild(NearbyPage),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyA9KsxPhnyl66IfUnGE_uYzw5fuSW0E5M4'
        }),
        TranslateModule.forChild()
    ],
    exports: [
        NearbyPage
    ]
})

export class NearbyPageModule { }
