import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { BannersService } from '../../providers/banners-service';

@IonicPage({
    name: 'page-order-confirmation',
    segment: 'order/confirmation'
})

@Component({
    selector: 'page-order-confirmation',
    templateUrl: 'order-confirmation.html'
})
export class OrderConfirmationPage {
    banner: any = null;

    constructor(
        public navCtrl: NavController,
        public bannerService: BannersService
    ) {
        this.bannerService.getItem().then(banner => {
            this.banner = banner;
        })
    }

    goToOrders() {
        this.navCtrl.setRoot('page-orders');
    }

}
