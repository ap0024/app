import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReviewAddPage } from './review-add';
import { Ionic2RatingModule } from 'ionic2-rating';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        ReviewAddPage
    ],
    imports: [
        IonicPageModule.forChild(ReviewAddPage),
        Ionic2RatingModule,
        TranslateModule.forChild()
    ],
    exports: [
        ReviewAddPage
    ]
})

export class ReviewAddPageModule { }
