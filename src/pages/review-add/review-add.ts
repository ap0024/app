import { Component } from '@angular/core';
import { IonicPage, NavParams, LoadingController, ToastController, NavController } from 'ionic-angular';
import { ProfileService } from '../../providers/profile-service';
import { ReviewsService } from '../../providers/reviews-service-mock';
import { TranslateService } from '@ngx-translate/core';

@IonicPage({
    name: 'page-review-add',
    segment: 'review/add/:orderId'
})

@Component({
    selector: 'page-review-add',
    templateUrl: 'review-add.html'
})
export class ReviewAddPage {

    orderId: number = 0;
    userId: number;
    review: any = {};
    loading: any = false;
    translation: any = {};

    constructor(
        public navParams: NavParams,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public navCtrl: NavController,
        public profileService: ProfileService,
        public reviewService: ReviewsService,
        public translate: TranslateService
    ) { }

    ngOnInit() { 
        this.translate.get([
            'Por favor, espere...'
            ]).subscribe((res: string) => {
            this.translation = res;
        });
        this.orderId = this.navParams.get('orderId');
    }

    ionViewDidLoad() {
        console.log(this);
    }

    onModelChange(e) {
        console.log(e);
    }

    save() {
        if (this.loading) {
            return;
        }
        this.loading = true;
        let loader = this.loadingCtrl.create({
            content: this.translation['Por favor, espere...']
        });
        loader.present();
        
        this.reviewService.saveReview(this.orderId, this.review).then(data => {
            this.loading = false;
            loader.dismiss();
            let toast = this.toastCtrl.create({
                message: "ERROR" in data ? data["ERROR"] : 'Se ha enviado su valoración.',
                duration: 3000,
                position: 'top',
                cssClass: '',
                showCloseButton: false
            });
            toast.present();
            this.translate.get("ERROR" in data ? data["ERROR"] : 'Se ha enviado su valoración.')
                .subscribe((error: string) => {
                this.toastCtrl.create({
                    showCloseButton: false,
                    cssClass: '',
                    message: error,
                    duration: 3000,
                    position: 'top',
                }).present();
            });
            this.navCtrl.setRoot('page-restaurant-list');
        })
            
        
    }

}
