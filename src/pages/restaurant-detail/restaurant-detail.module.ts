import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgmCoreModule } from '@agm/core';
import { RestaurantDetailPage } from './restaurant-detail';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        RestaurantDetailPage
    ],
    imports: [
        IonicPageModule.forChild(RestaurantDetailPage),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyA9KsxPhnyl66IfUnGE_uYzw5fuSW0E5M4'
        }),
        PipesModule,
        TranslateModule.forChild()
    ],
    exports: [
        RestaurantDetailPage
    ]
})

export class RestaurantDetailPageModule { }
