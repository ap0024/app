import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, ActionSheetController, ActionSheet, NavController, NavParams, LoadingController, ToastController, ModalController, Slides } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { RestaurantService } from '../../providers/restaurant-service-mock';

@IonicPage({
    name: 'page-restaurant-detail',
    segment: 'restaurant/:id'
})

@Component({
    selector: 'page-restaurant-detail',
    templateUrl: 'restaurant-detail.html'
})
export class RestaurantDetailPage implements OnInit {
    param: number;

    @ViewChild('imageSlides') imageSlides: Slides;
    @ViewChild('menuSlides') menuSlides: Slides;

    restaurant: any;
    restaurantopts: String = 'info';
    dishes: Array<any>;
    offerId: number = 0;
    loading: any = false;
    translation: any = {};  
    iconMarker = {
        url: '../../assets/icon/Ripple-3s-200px.svg',
        anchor: {
            x:50, 
            y:50
        },
        scaledSize: {
          width: 100,
          height: 100
        }
    };

    menuTypes: any = {
        appetizers: true,
        starters: false,
        main: false,
        desserts: false,
        drinks: false,
    }

    constructor(
        public loadingCtrl: LoadingController, 
        public modalCtrl: ModalController,
        public actionSheetCtrl: ActionSheetController,
        public navCtrl: NavController,
        public navParams: NavParams,
        public restaurantService: RestaurantService,
        public toastCtrl: ToastController,
        public translate: TranslateService
    ) { }

    getRestaurant(id) {
        if (this.loading) {
            return;
        }

        this.loading = true;
        let loader = this.loadingCtrl.create({
            content: this.translation['Por favor, espere...']
        });

        loader.present();
        if (!this.restaurant || (this.restaurant && this.restaurant.id !== id)) {
            this.restaurantService.getItem(id).then((restaurant) => {
                this.loading = false;
                loader.dismiss();
                this.restaurant = restaurant;
                this.dishes = this.restaurant ? this.restaurant.dishes : [];
            });
        }
    }

    openOffersModal() {
        let modal = this.modalCtrl.create('page-restaurant-offers', { 
            'restaurant': this.restaurant 
        });
        modal.onDidDismiss(offerId => {
            this.offerId = offerId;
            if (offerId != null) {
                this.openOrderDetail();
            }
        })
        modal.present();
    }

    slideTo(n) {
        this.menuSlides.slideTo(n);
    }

    showMenu(type) {
        this.menuTypes.appetizers = false;
        this.menuTypes.starters = false;
        this.menuTypes.main = false;
        this.menuTypes.desserts = false;
        this.menuTypes.drinks = false;

        this.menuTypes[type] = true;
    }

    ngOnInit() { 
        this.translate.get([
            'Por favor, espere...',
            'Restaurant added to your favorites'
            ]).subscribe((res: string) => {
            this.translation = res;
        });
        this.param = this.navParams.get('id');
        this.getRestaurant(this.param);
    }

    openDishDetail(dish) {
        this.navCtrl.push('page-dish-detail', {
            'id': dish.id
        });
    }

    openOrderDetail() {
        if (this.restaurant && this.restaurant.id) {
            this.navCtrl.push('page-order-add', {
                'restaurantId': this.restaurant.id,
                'offerId': this.offerId
            });
            this.offerId = 0;
        }
    }

    favorite(restaurant) {
        this.restaurantService.favorite(restaurant)
            .then(restaurant => {
                let toast = this.toastCtrl.create({
                    message: this.translation['Restaurant added to your favorites'],
                    cssClass: 'mytoast',
                    duration: 2000
                });
                toast.present(toast);
            });
    }

    share(restaurant) {
        let actionSheet: ActionSheet = this.actionSheetCtrl.create({
            title: 'Share via',
            buttons: [
                {
                    text: 'Twitter',
                    handler: () => console.log('share via twitter')
                },
                {
                    text: 'Facebook',
                    handler: () => console.log('share via facebook')
                },
                {
                    text: 'Email',
                    handler: () => console.log('share via email')
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => console.log('cancel share')
                }
            ]
        });

        actionSheet.present();
    }

    openCart() {
        console.log('page-cart-opening')
        this.navCtrl.push('page-cart');
    }

}
