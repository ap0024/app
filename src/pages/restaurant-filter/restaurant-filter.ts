import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController } from 'ionic-angular';
import { FilterService } from '../../providers/filter-service'
import { RestaurantService } from '../../providers/restaurant-service-mock';
@IonicPage({
    name: 'page-restaurant-filter',
    segment: 'restaurant-filter'
})

@Component({
    selector: 'page-restaurant-filter',
    templateUrl: 'restaurant-filter.html',
})

export class RestaurantFilterPage {

    filterPrices: any;
    filterTypes: any;
    filterCharacteristics: any;
    filterDistance: number = 0;

    data: any = {};

    constructor(
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public filterService: FilterService,
        public restaurantService: RestaurantService,
    ) { 
        this.data = this.filterService.filters;
        this.filterPrices = this.filterService.prices;
        this.filterService.loadTypes().then(data => this.filterTypes = data);
        this.filterService.loadCharacteristics().then(data => this.filterCharacteristics = data);
        this.filterDistance = restaurantService.distance;
    }

    closeModal() {
        // this.navCtrl.pop();
        this.filter();
    }

    filter() {
        this.filterService.filters = Object.assign(this.filterService.filters, this.data);
        this.restaurantService.distance = this.filterDistance;
        this.viewCtrl.dismiss(this.filterService.filters);
    }

}
