import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantFilterPage } from './restaurant-filter';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [
		RestaurantFilterPage
	],
	imports: [
		IonicPageModule.forChild(RestaurantFilterPage),
        TranslateModule.forChild()
	],
	exports: [
		RestaurantFilterPage
	]
})

export class RestaurantFilterPageModule { }
