import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { IonicPage, NavController, LoadingController, ToastController } from 'ionic-angular';
import { ProfileService } from '../../providers/profile-service';
import { Events } from 'ionic-angular';
import {TranslateService} from '@ngx-translate/core';

@IonicPage({
    name: 'page-auth-register',
    segment: 'auth-register'
})

@Component({
    selector: 'page-auth-register',
    templateUrl: 'auth-register.html',
})

export class AuthRegisterPage implements OnInit {

    data: any = {
        'fullName': '',
        'email': '',
        'password': ''
    };

    public onRegisterForm: FormGroup;
    loading: any = false;

    translation: any = {};
    
    constructor(
        public navCtrl: NavController,
        private _fb: FormBuilder,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public profileService: ProfileService,
        public events: Events,
        public translate: TranslateService
    ) { }

    closeModal() {
        this.navCtrl.pop();
    }

    ngOnInit() {
        this.translate.get([
            'Por favor, espere...',
            'Se ha registrado correctamente.'
            ]).subscribe((res: string) => {
            this.translation = res;
        });
        this.onRegisterForm = this._fb.group({
            fullName: ['', Validators.compose([
                Validators.required
            ])],
            email: ['', Validators.compose([
                Validators.required
            ])],
            password: ['', Validators.compose([
                Validators.required
            ])]
        });
    }

    register() {
        if (this.loading) {
            return;
        }
        this.loading = true;
        let loader = this.loadingCtrl.create({
            content: this.translation['Por favor, espere...']
        });
        // show message
        let toast = this.toastCtrl.create({
            showCloseButton: false,
            cssClass: '',
            message: this.translation['Se ha registrado correctamente.'],
            duration: 3000,
            position: 'top',
        });

        loader.present();

        let apiErrors = [
            "ERROR", "email" , "password", "USER_DATA.DATA_NAME"
        ]

        this.profileService.register(this.data).then(result => {
            this.loading = false;
            loader.dismiss();

            if (this.profileService.getCurrentUserToken()) {
                toast.present();
                this.closeModal();

            } else {
                apiErrors.filter(m => m in result ? this.toastCtrl.create({
                    showCloseButton: false,
                    cssClass: '',
                    message: result[m],
                    duration: 3000,
                    position: 'top',
                }).present() : null);
            }

        })
    }

}
