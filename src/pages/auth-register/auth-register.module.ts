import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AuthRegisterPage } from './auth-register';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        AuthRegisterPage
    ],
    imports: [
        IonicPageModule.forChild(AuthRegisterPage),
        TranslateModule.forChild()
    ],
    exports: [
        AuthRegisterPage
    ]
})

export class AuthRegisterPageModule { }
