import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, Slides, NavController, MenuController } from 'ionic-angular';
import {TranslateService} from '@ngx-translate/core';

@IonicPage({
    name: 'page-walkthrough',
    segment: 'walkthrough',
    priority: 'high'
})

@Component({
    selector: 'page-walkthrough',
    templateUrl: 'walkthrough.html',
})
export class WalkthroughPage {
    @ViewChild(Slides) slides: Slides;
    @ViewChild('introVideo', {read: ElementRef}) private introVideo : ElementRef;

    showSkip = true;
    dir: string = 'ltr';

    translation: any;

    slideList: Array<any> = [];

    constructor(
        public navCtrl: NavController,
        public menu: MenuController,
        public translate: TranslateService
    ) {
        this.menu.swipeEnable(false);
        this.menu.enable(false);

        localStorage.setItem('tutorial', '1');
    }


    ngOnInit() {
        console.log('ngOnInit Auth Language', this.translate.currentLang);
        this.translate.get([
            'EXPLORA',
            'Una selección de las mejores terrazas de restaurantes y bares',
            'RESERVA',
            'Fácilmente según tus gustos y preferencias',
            'BENEFICIATE',
            'De nuestro programa de descuentos en tu cuenta final'
            ]).subscribe((res: string) => {
                console.log('translations', res);
                this.translation = res;

        });

        
        this.slideList = [

            {
                source: 'assets/video/intro.mp4'
            },
            {
                title: this.translation["EXPLORA"],
                description: this.translation["Una selección de las mejores terrazas de restaurantes y bares"],
                image: "assets/img/intro/intro01.jpg",
            },
            {
                title: this.translation["RESERVA"],
                description: this.translation["Fácilmente según tus gustos y preferencias"],
                image: "assets/img/intro/intro02.jpg",
            },
            {
                title: this.translation["BENEFICIATE"],
                description: this.translation["De nuestro programa de descuentos en tu cuenta final"],
                image: "assets/img/intro/intro03.jpg",
            },
        
        ];
        

    }

    slideChanged() {
        let currentIndex = this.slides.getActiveIndex() || 0;
        if (this.slideList[currentIndex] && this.slideList[currentIndex].source) {
            this.introVideo.nativeElement.play();
        } else {
            this.introVideo.nativeElement.pause();
        }
    }
    slideDragging() {
        let currentIndex = this.slides.getActiveIndex() || 0;
        if (this.slideList[currentIndex] && this.slideList[currentIndex].source) {
            this.introVideo.nativeElement.pause();
        }
    }

    onSlideNext() {
        this.slides.slideNext(300)
    }

    onSlidePrev() {
        this.slides.slidePrev(300)
    }

    onLastSlide() {
        this.slides.slideTo(3, 300)
    }

    openHomePage() {
        this.navCtrl.setRoot('page-restaurant-list');

        // localStorage.setItem('token', '1');
    }

    openAuthPage() {
        this.navCtrl.setRoot('page-auth');
    }

    ionViewDidLoad() {
        this.slideChanged();
    }

}
