import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { IonicPage, NavController, LoadingController, AlertController, ToastController, MenuController, ModalController } from 'ionic-angular';
import { ProfileService } from '../../providers/profile-service';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Events } from 'ionic-angular';
import {TranslateService} from '@ngx-translate/core';
import { availableLanguages } from '../../providers/config';

@IonicPage({
    name: 'page-auth',
    segment: 'auth',
    priority: 'high'
})

@Component({
    selector: 'page-auth',
    templateUrl: 'auth.html'
})
export class AuthPage implements OnInit {
    public onLoginForm: FormGroup;
    public onRegisterForm: FormGroup;
    public loader;
    auth: string = "login";
    public data: any = {
        'email': '',
        'password': ''
    };

    render: boolean = false;
    passwordType: string = 'password';
    passwordIcon: string = 'eye-off';

    loading: any = false;

    translation: any = {};

    availableLanguages = availableLanguages;

    constructor(
        private _fb: FormBuilder,
        public nav: NavController,
        public forgotCtrl: AlertController,
        public menu: MenuController,
        public toastCtrl: ToastController,
        public modalCtrl: ModalController,
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        private profileService: ProfileService,
        private fb: Facebook,
        public events: Events,
        public translate: TranslateService
    ) {
        this.menu.swipeEnable(false);
        this.menu.enable(false);

        if (this.profileService.getCurrentUserToken()) {
            this.openHomePage();
        } else {
            this.render = true;
        }

    }

    openRegisterPage() {
        let modal = this.modalCtrl.create('page-auth-register');
        modal.onDidDismiss(() => {
            if (this.profileService.getCurrentUserToken()) {
                this.events.publish('toggleProfile');
                this.openHomePage();
            }
        });
        modal.present();
    }

    toggleLanguage(language) {
        console.log('auth.toggleLanguage', language.code)
        this.events.publish('toggleLanguage', language.code);
        this.navCtrl.setRoot('page-auth');
    }

    openHomePage() {
        this.navCtrl.setRoot('page-restaurant-list');
    }

    ngOnInit() {
        console.log('ngOnInit Auth Language', this.translate.currentLang);
        this.translate.get([
            'Por favor, espere...',
            'Inicia sesión con facebook fallido',
            'Nombre de usuario o contraseña incorrectos',
            'Se ha enviado un correo electrónico para recuperar la contraseña.',
            '¿Olvidaste la contraseña?',
            'Introduce tu correo electrónico para enviar una enlace para restablecer la contraseña.',
            'Correo electrónico',
            'Cancelar',
            'La verificación es correcta',
            '¿Recibiste un token?',
            'Introduce tu token personal para restablecer la contraseña.',
            'Ficha de verificación',
            'Contraseña cambiada. Puedes usar tu nueva contraseña para iniciar sesión.',
            'Restablecer la contraseña',
            'Contraseña',
            'Confirmar contraseña'
            ]).subscribe((res: string) => {
                console.log('translations', res);
                this.translation = res;
        });
        
        this.onLoginForm = this._fb.group({
            email: ['', Validators.compose([
                Validators.required
            ])],
            password: ['', Validators.compose([
                Validators.required
            ])]
        });

        this.onRegisterForm = this._fb.group({
            fullName: ['', Validators.compose([
                Validators.required
            ])],
            email: ['', Validators.compose([
                Validators.required
            ])],
            password: ['', Validators.compose([
                Validators.required
            ])]
        });

    }

    // open homepage with facebook

    openFacebook() {
        if (this.loading) {
            return;
        }
        this.loading = true;
        let loader = this.loadingCtrl.create({
            content: this.translation['Por favor, espere...']
        });
        let error = this.toastCtrl.create({
            showCloseButton: false,
            cssClass: '',
            message: this.translation['Inicia sesión con facebook fallido'],
            duration: 3000,
            position: 'top',
        })
        loader.present();
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then((res: FacebookLoginResponse) => {
                this.loading = false;
                loader.dismiss();
                if(res.status === "connected" && res.authResponse.userID) {
                    let userId = res.authResponse.userID;
                    this.registerOrLoginFacebook(userId);
                    
                } else {

                    error.present();
                    console.error('Error logging into Facebook', res)
                }
            })
            .catch(e => {
                loader.dismiss();

                error.present();
                console.error('Error logging into Facebook', e)
            });
    }

    registerOrLoginFacebook(userId) {
        if (this.loading) {
            return;
        }
        this.loading = true;
        let loader = this.loadingCtrl.create({
            content: this.translation['Por favor, espere...']
        });

        loader.present();
        let error = 'Inicia sesión con facebook fallido';
        this.fb.api("/"+userId+"/?fields=email,name",["public_profile"])
            .then(res => {
                loader.dismiss();
                this.profileService.loginFaceBook(userId, res).then(() => {
                    this.loading = false;
                    if (this.profileService.getCurrentUserToken()) {
                        this.events.publish('toggleProfile');
                        this.openHomePage();
                    } else {
                        this.loading = true;
                        let registerLoader = this.loadingCtrl.create({
                            content: this.translation['Por favor, espere...']
                        });
                        registerLoader.present();
                        this.profileService.registerFaceBook(userId, res).then((result)=>{
                            this.loading = false;
                            registerLoader.dismiss();
                            if (this.profileService.getCurrentUserToken()) {
                                this.events.publish('toggleProfile');
                                this.openHomePage();
                            } else {
                                let apiErrors = ["ERROR", "email" , "password", "USER_DATA.DATA_NAME"];
                                let apiError = apiErrors.find(m => m in result);
                                this.toastCtrl.create({
                                    showCloseButton: false,
                                    cssClass: '',
                                    message: apiError ? result[apiError] : error,
                                    duration: 3000,
                                    position: 'top',
                                }).present();
                            }
                        })
                    }
                })
                
                console.log('Logged into Facebook!', res);
            })
            .catch(e => {
                loader.dismiss();
                console.log('Error getting details from Facebook', e)
                
            });
    }

    // login and go to home page or show error
    login() {
        if (this.loading) {
            return;
        }
        this.loading = true;
        let loader = this.loadingCtrl.create({
            content: this.translation['Por favor, espere...']
        });
        let toast = this.toastCtrl.create({
            showCloseButton: false,
            cssClass: '',
            message: this.translation['Nombre de usuario o contraseña incorrectos'],
            duration: 3000,
            position: 'top',
        })

        loader.present();

        this.profileService.login(this.data).then(() => {

            this.loading = false;
            loader.dismiss();
            if (this.profileService.getCurrentUserToken()) {
                this.events.publish('toggleProfile');
                this.openHomePage();
            } else {
                toast.present();
            }
        });
    }

    forgotPass() {
        let successText = this.translation['Se ha enviado un correo electrónico para recuperar la contraseña.'];
                     
        let forgotPass = this.forgotCtrl.create({
            title: this.translation['¿Olvidaste la contraseña?'],
            message: this.translation['Introduce tu correo electrónico para enviar una enlace para restablecer la contraseña.'],
            inputs: [
                {
                    name: 'email',
                    placeholder: this.translation['Correo electrónico'],
                    type: 'email'
                },
            ],
            buttons: [
                {
                    text: this.translation['Cancelar'],
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Enviar',
                    handler: data => {
                        if (data.email && data.email.length > 0) {
                            if (this.loading) {
                                return;
                            }
                            this.loading = true;           
                            let loader = this.loadingCtrl.create({
                                content: this.translation['Por favor, espere...']
                            });
                            loader.present();
                            this.profileService.forgotPass(data.email).then((response) => {
                                this.loading = false;
                                loader.dismiss();
                                let toast = this.toastCtrl.create({
                                    message: 'ERROR' in response ? response['ERROR'] : successText,
                                    duration: 3000,
                                    position: 'top',
                                    cssClass: 'dark-trans',
                                    showCloseButton: false
                                });
                                toast.present();
                                if ('success' in response) {
                                    this.verifyToken(data.email);
                                } else {
                                    this.forgotPass();
                                }
                            })
                        }
                    }
                }
            ]
        });

        forgotPass.present();
    }
    
    verifyToken(email) {
        let successText = this.translation['La verificación es correcta'];     
        let verifyToken = this.forgotCtrl.create({
            title: this.translation['¿Recibiste un token?'],
            message: this.translation['Introduce tu token personal para restablecer la contraseña.'],
            inputs: [
                {
                    name: 'token',
                    placeholder: this.translation['Ficha de verificación'],
                    type: 'text'
                },
            ],
            buttons: [
                {
                    text: 'No recibió',
                    handler: data => {
                        this.forgotPass();
                    }
                },
                {
                    text: 'Verificar',
                    handler: data => {
                        if (data.token && data.token.length > 0) {
                            if (this.loading) {
                                return;
                            }
                            this.loading = true;                
                            let loader = this.loadingCtrl.create({
                                content: this.translation['Por favor, espere...']
                            });
                            loader.present();
                            this.profileService.verifyToken({
                                'token': data.token, 
                                'email': email
                            }).then(response => {
                                this.loading = false;
                                loader.dismiss();
                                let toast = this.toastCtrl.create({
                                    message: 'ERROR' in response ? response['ERROR'] : successText,
                                    duration: 3000,
                                    position: 'top',
                                    cssClass: 'dark-trans',
                                    showCloseButton: false
                                });
                                toast.present();
                                if ('success' in response) {
                                    this.resetPassword(email, data.token);
                                } else {
                                    this.forgotPass();
                                }
                            })
                        }
                    }
                }
            ]
        });
        
        verifyToken.present();
    }

    resetPassword(email, token) {
        let successText = this.translation['Contraseña cambiada. Puedes usar tu nueva contraseña para iniciar sesión.'];                 
        
        let resetPassword = this.forgotCtrl.create({
            title: this.translation['Restablecer la contraseña'],
            message: 'Introduce tu contraseña y confírmala.',
            inputs: [
                {
                    name: 'password',
                    placeholder: this.translation['Contraseña'],
                    type: 'password'
                },
                {
                    name: 'password_confirmation',
                    placeholder: this.translation['Confirmar contraseña'],
                    type: 'password'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.translation['Restablecer la contraseña'],
                    handler: data => {
                        if (data.password && data.password.length > 0 
                            && data.password_confirmation && data.password_confirmation.length > 0) {
                            if (this.loading) {
                                return;
                            }
                            this.loading = true;
                            let loader = this.loadingCtrl.create({
                                content: this.translation['Por favor, espere...']
                            });
                            loader.present();
                            this.profileService.resetPassword({
                                'token': token, 
                                'email': email,
                                'password': data.password,
                                'password_confirmation': data.password_confirmation
                            }).then((response) => {
                                this.loading = false;
                                loader.dismiss();
                                let toast = this.toastCtrl.create({
                                    message: 'ERROR' in response ? response['ERROR'] : successText,
                                    duration: 3000,
                                    position: 'top',
                                    cssClass: 'dark-trans',
                                    showCloseButton: false
                                });
                                toast.present();

                                if (this.profileService.getCurrentUserToken()) {
                                    this.events.publish('toggleProfile');
                                    this.openHomePage();
                                } else {
                                    this.resetPassword(email, token);
                                }
                            })
                        }
                    }
                }
            ]
        });

        resetPassword.present();
    }


    hideShowPassword() {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }

}
