import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AuthPage } from './auth';
import { ProfileService } from '../../providers/profile-service'
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
    declarations: [
        AuthPage
    ],
    imports: [
        IonicPageModule.forChild(AuthPage),
        TranslateModule.forChild()
    ],
    exports: [
        AuthPage
    ],
    providers: [
        ProfileService
    ]
})

export class AuthPageModule { }
