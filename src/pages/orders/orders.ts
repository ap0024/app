import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { OrdersService } from '../../providers/orders-service-mock';
import {TranslateService } from '@ngx-translate/core';
@IonicPage({
    name: 'page-orders',
    segment: 'orders'
})

@Component({
    selector: 'page-orders',
    templateUrl: 'orders.html',
})

export class OrdersPage {

    lastOrders: Array<any> = [];
    loading: any = false;
    translation: any = {};

    constructor(
        public loadingCtrl: LoadingController, 
        public navCtrl: NavController,
        public navParams: NavParams,
        public ordersService: OrdersService,
        public alertCtrl: AlertController,
        public toastCtrl: ToastController,
        public translate: TranslateService
    ) { }

    ngOnInit() {
        this.translate.get([
            'Por favor, espere...',
            'Se ha realizado la reserva correctamente.',
            'Confirmar reserva',
            '¿Desea confirmar la reserva?',
            'No',
            'Sí',
            'Cancelar reserva',
            '¿Desea cancelar la reserva?',
            ]).subscribe((res: string) => {
            this.translation = res;
        });
        this.getOrders();
    }

    getOrders() {
        if (this.loading) {
            return; 
        }
        this.loading = true;
        let loader = this.loadingCtrl.create({
            content: this.translation['Por favor, espere...']
        });

        loader.present();
        this.ordersService.getAllOrders()
            .then(data => {
                this.loading = false;
                loader.dismiss();
                this.lastOrders = data
            });
    }

    addReview(order) {
        this.navCtrl.push('page-review-add', {
            'orderId': order.id
        });
    }

    confirmOrder(order) {

        this.alertCtrl.create({
            title: this.translation['Confirmar reserva'],
            message: this.translation['¿Desea confirmar la reserva?'],
            inputs: [],
            buttons: [
                {
                    text: this.translation['No'],
                    handler: data => {
                        this.removeOrder(order.id);
                        if (order.offer) {
                            this.ordersService.cancelOffer(order.id).then(_ => this.cancelHandler(_))
                        } else {
                            this.ordersService.cancelOrder(order.id).then(_ => this.cancelHandler(_));
                        }
                    }
                },
                {
                    text: this.translation['Sí'],
                    handler: data => {
                        this.confirmOrderInList(order.id);
                        if (order.offer) {
                            this.ordersService.confirmOffer(order.id).then(_ => this.confirmHandler(_))
                        } else {
                            this.ordersService.confirmOrder(order.id).then(_ => this.confirmHandler(_));
                        }
                        
                        
                    }
                }
            ]
        }).present();
    }

    confirmOrderInList(id) {
        let index = this.lastOrders.findIndex(item => item.id === id);
        this.lastOrders[index].toConfirm = false;
    }
    removeOrder(id) {
        let index = this.lastOrders.findIndex(item => item.id === id);
        this.lastOrders[index].removed = true;
        setTimeout( () => {
            if (index > -1) {
                this.lastOrders.splice(index, 1);
            }
       }, 1000);
    }

    deleteOrder(order) {
        this.removeOrder(order.id);
        if (order.offer) {
            this.ordersService.deleteOffer(order.id);
        } else {
            this.ordersService.deleteOrder(order.id);
        }
    }

    cancelOrder(order) {
        let cancel = this.alertCtrl.create({
            title: this.translation['Cancelar reserva'],
            message: this.translation['¿Desea cancelar la reserva?'],
            inputs: [],
            buttons: [
                {
                    text: 'No',
                    handler: data => {
                        console.log('La reserva no se ha cancelado.');
                    }
                },
                {
                    text: 'Sí',
                    handler: data => {
                        this.removeOrder(order.id);
                        if (order.offer) {
                            this.ordersService.cancelOffer(order.id).then(_ => this.cancelHandler(_))
                        } else {
                            this.ordersService.cancelOrder(order.id).then(_ => this.cancelHandler(_));
                        }
                        
                        
                    }
                }
            ]
        });
        cancel.present();
    }

    openRestaurant(restaurantId) {
        this.navCtrl.push('page-restaurant-detail', {
            'id': restaurantId
        });
    }

    cancelHandler(data) {
        this.translate.get("ERROR" in data ? data["ERROR"] : 'Se ha cancelado la reserva')
            .subscribe((error: string) => {
            this.toastCtrl.create({
                showCloseButton: false,
                cssClass: '',
                message: error,
                duration: 3000,
                position: 'top',
            }).present();
        });
    }
    
    confirmHandler(data) {
        this.translate.get("ERROR" in data ? data["ERROR"] : 'Se ha conirmado la reserva')
            .subscribe((error: string) => {
            this.toastCtrl.create({
                showCloseButton: false,
                cssClass: '',
                message: error,
                duration: 3000,
                position: 'top',
            }).present();
        });
    }

}
