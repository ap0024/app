import { Component } from '@angular/core';
import { IonicPage, Config, NavController, LoadingController, NavParams, ToastController, ModalController, MenuController } from 'ionic-angular';
import { RestaurantService } from '../../providers/restaurant-service-mock';
import { Geolocation } from '@ionic-native/geolocation';
import { ProfileService } from '../../providers/profile-service';
import { TranslateService } from '@ngx-translate/core';
@IonicPage({
    name: 'page-restaurant-list',
    segment: 'restaurant-list'
})

@Component({
    selector: 'page-restaurant-list',
    templateUrl: 'restaurant-list.html'
})
export class RestaurantListPage {

    restaurants: Array<any>;
    searchKey: string = "";
    viewMode: string = "list";
    proptype: string;
    label: string = "";
    from: string;
    lat: number = undefined;
    lng: number = undefined;
    maplat: number = undefined;
    maplng: number = undefined;
    filter: any = {
        price: [],
        types: [],
        characteristics: []
    }
    filterargs: { title: 'hello' };
    offers: any = false;
    loading: any = false;
    translation: any = {}; 
    iconMarker = {
        url: '../../assets/icon/Ripple-3s-200px.svg',
        anchor: {
            x:50, 
            y:50
        },
        scaledSize: {
          width: 100,
          height: 100
        }
    };

    constructor(
        public loadingCtrl: LoadingController, 
        public navCtrl: NavController,
        public navParams: NavParams,
        public service: RestaurantService,
        public toastCtrl: ToastController,
        public modalCtrl: ModalController,
        public config: Config,
        public menuCtrl: MenuController,
        private geolocation: Geolocation,
        public profileService: ProfileService,
        public translate: TranslateService
    ) { }


    ngOnInit() { 
        this.translate.get([
            'Por favor, espere...'
            ]).subscribe((res: string) => {
            this.translation = res;
        });

        this.menuCtrl.enable(true);

        this.proptype = this.navParams.get('proptype') || 'Oritental';
        this.from = this.navParams.get('from') || "";

        if (!this.profileService.getCurrentUserToken()) {
            this.navCtrl.push('page-auth');
        }

        let geolocationEnabled = false;
        console.log('getCurrentPosition?')
        this.geolocation.getCurrentPosition()
            .then((data) => {
                geolocationEnabled = true;
                this.updateRestaurantListWithCoords(data.coords.latitude, data.coords.longitude);
            })
            .catch((error) => {
                console.log('findAll from position catch error')
                this.findAll();
            });

        if (geolocationEnabled) {
            let watch = this.geolocation.watchPosition();
            watch.subscribe((data) => {
                this.updateRestaurantListWithCoords(data.coords.latitude, data.coords.longitude);
            });
        } else {
            console.log('findAll from Else')
            this.findAll();
        }

    }

    updateRestaurantListWithCoords(lat, lng) {
        if (this.lat !== lat && this.lng !== lng) {

            this.lat = lat;
            this.lng = lng;

            this.maplat = this.lat;
            this.maplng = this.lng;
    
            this.findAll();
        }
    }

    openFilterModal() {
        this.restaurants = [];
        let modal = this.modalCtrl.create('page-restaurant-filter');
        modal.onDidDismiss(data => {
            this.findAll();
            if (data) {
                this.filter.price = data.price;
                this.filter.types = data.types;
                this.filter.characteristics = data.characteristics;
            }
        })
        modal.present();
    }

    openRestaurantDetail(restaurant: any) {
        this.navCtrl.push('page-restaurant-detail', {
            'id': restaurant.id
        });
    }

    favorite(restaurant) {
        this.service.favorite(restaurant)
            .then(restaurant => {
                let toast = this.toastCtrl.create({
                    message: 'Property added to your favorites',
                    cssClass: 'mytoast',
                    duration: 2000
                });
                toast.present(toast);
            });
    }

    onInput(event) {
        this.service.findByName(this.searchKey)
            .then(data => {
                this.restaurants = data;
            })
            .catch(error => {
                
            });
    }

    onCancel(event) {
        this.findAll();
    }

    findAll() {
        if (this.loading) {
            return;
        }
        this.loading = true;
        this.restaurants = [];

        let loader = this.loadingCtrl.create({
            content: this.translation['Por favor, espere...']
        });

        loader.present();

        this.service.findAll(this.lng, this.lat)
            .then(data => {

                this.loading = false;
                loader.dismiss();
                data.reverse();
                data.forEach(item => {
                    if (item.offers.length > 0) {
                        this.offers = true;
                    }
                    if (this.lat == undefined && this.lng == undefined
                        && this.maplat == undefined && this.maplng == undefined
                        && item.lat !== undefined && item.long !== undefined) {
                        this.maplat = item.lat;
                        this.maplng = item.long;
                    }
                })
                this.restaurants = data;
            })
            .catch(error => {
                loader.dismiss();
            });
    }

}
