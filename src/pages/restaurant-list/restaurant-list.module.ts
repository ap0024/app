import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantListPage } from './restaurant-list';

import { AgmCoreModule } from '@agm/core';
import { PipesModule } from '../../pipes/pipes.module';
import { Geolocation } from '@ionic-native/geolocation';
import { ProfileService } from '../../providers/profile-service'
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
    declarations: [
        RestaurantListPage
    ],
    imports: [
        IonicPageModule.forChild(RestaurantListPage),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyA9KsxPhnyl66IfUnGE_uYzw5fuSW0E5M4'
        }),
        PipesModule,
        TranslateModule.forChild()
    ],
    exports: [
        RestaurantListPage
    ],
    providers: [
        Geolocation,
        ProfileService
    ]
})

export class RestaurantListPageModule { }
