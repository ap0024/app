import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantOffersPage } from './restaurant-offers';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        RestaurantOffersPage
    ],
    imports: [
        IonicPageModule.forChild(RestaurantOffersPage),
        TranslateModule.forChild()
    ],
    exports: [
        RestaurantOffersPage
    ]
})

export class RestaurantOffersPageModule { }
