import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { RestaurantService } from '../../providers/restaurant-service-mock';

@IonicPage({
    name: 'page-restaurant-offers',
    segment: 'restaurant-offers/:restaurantId'
})

@Component({
    selector: 'page-restaurant-offers',
    templateUrl: 'restaurant-offers.html',
})

export class RestaurantOffersPage {

    param: number;
    restaurant: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController,
        public restaurantService: RestaurantService,
        public loadingCtrl: LoadingController
    ) {
        this.restaurant = this.navParams.get('restaurant');
    }

    addOrder(offerId) {
        this.viewCtrl.dismiss(offerId);
    }

    closeModal() {
        this.navCtrl.pop();
    }

}
