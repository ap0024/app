import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, ModalController } from 'ionic-angular';

import { CartService } from '../../providers/cart-service-mock';
import { RestaurantService } from '../../providers/restaurant-service-mock';
import { OrdersService } from '../../providers/orders-service-mock';
import { TranslateService } from '@ngx-translate/core';

@IonicPage({
    name: 'page-order-add',
    segment: 'order/:restaurantId/:offerId'
})

@Component({
    selector: 'page-order-add',
    templateUrl: 'order-add.html'
})
export class OrderAddPage {
    restaurantId: number;
    restaurantName: string;

    offerId: number;
    offerName: string;

    offer: any = null;

    date: any = '';
    hour: any = '';

    restaurant: any;
    qtd: number = 2;
    showDays: number = 30;
    unlimitedStock: number = 999;

    checkoutData: any;
    paymethods: string = 'creditcard';
    totalVal: number = 0;
    orderNumber: number = Math.floor(Math.random() * 10000);

    days: Array<string> = [];
    hours: any[] = [];

    tables = [];
    stock = [];

    limit = 0;

    loader;
    toast;

    loading: any = false;

    translation: any = {};

    constructor(
        public modalCtrl: ModalController,
        public navCtrl: NavController,
        public navParams: NavParams,
        public toastCtrl: ToastController,
        public cartService: CartService,
        public restaurantService: RestaurantService,
        public loadingCtrl: LoadingController,
        public ordersService: OrdersService,
        public translate: TranslateService
    ) { }


    ngOnInit() {

        this.days = [];
        this.tables = [];

        this.restaurantId = this.navParams.get('restaurantId');
        this.offerId = this.navParams.get('offerId');

        this.translate.get([
            'Por favor, espere...',
            'Se ha realizado la reserva correctamente.'
        ]).subscribe((res: string) => {
            this.translation = res;
        });
        this.restaurantService.getItem(this.restaurantId).then(restaurant => {
            this.restaurant = restaurant;
            this.restaurantService.getOffer(restaurant, this.offerId).then(offer => {
                this.offer = offer;

                let now = new Date();
                this.days.push(now.toISOString().substr(0, 10));

                for (let i = 0; i < this.showDays; i++) {

                    let a = now.setDate(now.getDate() + 1);
                    let b = new Date(a);
                    let c = b.toISOString().substr(0, 10);

                    this.days.push(c);
                }
                this.date = this.days[Math.round((this.days.length - 1) / 2)];
                this.updateStocks();

            });
        });

        if (this.offerId == 0) {
            this.loading = true;
            let loader = this.loadingCtrl.create({
                content: this.translation['Por favor, espere...']
            });
            loader.present();
            this.ordersService.getTables(this.restaurantId, this.showDays)
                .then(tables => {
                    this.loading = false;
                    loader.dismiss();
                    if (tables.length > 0) {
                        this.qtd = 2; // set default for 2 people if reserving a table
                        this.tables = tables;
                        this.days = (this.tables || []).filter(x =>
                            (x.stocks || []).filter(
                                y => y.available > 0)
                                .length > 0)
                            .map(s => s.date);
                        this.date = this.days[Math.round((this.days.length - 1) / 2)];
                    }
                    this.updateStocks();
                });
        } else {
            this.loading = true;
            let loader = this.loadingCtrl.create({
                content: this.translation['Por favor, espere...']
            });
            loader.present();
            this.ordersService.getPromotionStock(this.restaurantId, this.offerId, this.showDays)
                .then(promotionStock => {
                    this.loading = false;
                    loader.dismiss();
                    if (promotionStock.length > 0) {
                        this.qtd = 1;
                        this.tables = promotionStock;
                        this.days = (this.tables || []).filter(x =>
                            (x.stocks || []).filter(
                                y => y.available > 0)
                                .length > 0)
                            .map(s => s.date);
                        this.date = this.days[Math.round((this.days.length - 1) / 2)];
                    }
                    this.updateStocks();
                });
        }

    }

    updateStocks() {
        this.stock = [];
        this.hours = [];
        if (this.date && this.tables.length > 0) {
            this.stock = this.tables.find(s => s.date == this.date).stocks.filter(a => a.available > 0);
            this.hours = (this.stock || []).map(h => h.time.substr(0, 5));
        }
        this.hour = this.hour || this.hours[Math.round((this.hours.length - 1) / 2)];
        let todayLimit = this.getAvailableLimit();
        if (this.qtd > todayLimit) {
            this.qtd = todayLimit;
        }
        if (this.qtd == 0) {
            this.qtd = todayLimit >= 2 ? 2 : todayLimit;
        }
    }

    changeDateTime($event) {
        this.updateStocks();
    }

    // minus adult when click minus button and quantity more then 0
    minusQtd() {
        if (this.qtd > 1) {
            this.qtd--;
        } else {
            this.qtd = 1;
        }
    }

    // plus adult when click plus button and quantity less then available limit
    plusQtd() {
        if (this.qtd < this.getAvailableLimit()) {
            this.qtd++
        } else {
            this.qtd = this.getAvailableLimit();
        }
    }

    getAvailableLimit() {
        this.limit = 0;
        if (this.stock.length > 0) {
            let tables = this.stock.find(l => l.time.substr(0, 5) === this.hour)

            this.limit = tables.available;

        }
        return this.limit;
    }

    send() {
        if (this.loading) {
            return;
        }
        this.loading = true;
        this.loader = this.loadingCtrl.create({
            content: this.translation['Por favor, espere...']
        });
        // show message
        this.toast = this.toastCtrl.create({
            showCloseButton: false,
            // cssClass: 'profile-bg',
            cssClass: '',
            message: this.translation['Se ha realizado la reserva correctamente.'],
            duration: 3000,
            position: 'top',

        });

        this.loader.present();

        console.log(this.date, this.hour);

        if (this.offer) {
            this.ordersService
                .orderOffer(this.restaurantId, this.offerId, this.date, this.hour, this.qtd)
                .then(r => this.sendSuccess(r))
                .catch(e => this.errorHandler(e));
        } else {
            this.ordersService
                .orderTables(this.restaurantId, this.date, this.hour, this.qtd)
                .then(r => this.sendSuccess(r))
                .catch(e => this.errorHandler(e));
        }

    }

    errorHandler(result) {
        this.loading = false;
        this.loader.dismiss();
        console.log('Error', result);
    }

    sendSuccess(result) {
        this.loading = false;
        this.loader.dismiss();
        if ("ERROR" in result) {
            this.translate.get(result["ERROR"]).subscribe((error: string) => {
                this.toastCtrl.create({
                    showCloseButton: false,
                    cssClass: '',
                    message: error,
                    duration: 3000,
                    position: 'top',
                }).present();
            });
        } else {
            this.toast.present();
            this.navCtrl.push('page-order-confirmation');
        }
    }

}
