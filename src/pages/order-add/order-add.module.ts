import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderAddPage } from './order-add';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        OrderAddPage
    ],
    imports: [
        IonicPageModule.forChild(OrderAddPage),
        TranslateModule.forChild()
    ],
    exports: [
        OrderAddPage
    ]
})

export class OrderAddPageModule { }
